# Version de l'application
version = "0.0.1"


def get_versions():
    return {
        "version": version,
        "full-revisionid": None,
        "dirty": None,
        "error": "unable to compute version",
        "date": None
    }
