from setuptools import setup
from versioner import get_version

requirements : list = None

# Maybe changing when requirements.txt will be ok
with open ('../requirements.txt', encoding="utf-16") as f:
    requirements = f.read().splitlines()


setup(
    name='vehicul electric',
    version=get_version(),
    author='personal',
    author_email='ibrahim.bouhaj@orange.com',
    description='VE 2024',
    keywords='ve_2024',
    python_requires='>=3.8',
    license='personal',
    url='',
    packages=[
        've',
        ],
    install_requires=requirements,
    entry_points={
        'console_scripts': [
        ]
    },
)