import re
import os


class NotThisMethod(Exception):
    pass


def get_version() -> str:

    try:
        with open(os.path.join(os.path.dirname(__file__), 've', '_version.py')) as f:
            contents = f.read()

    except OSError:
        raise NotThisMethod("unable to read _version.py")

    mo = re.search(r"version\s*=\s*\"(.*)\"\n", contents, re.M | re.S)

    if not mo:
        raise NotThisMethod("no version_json in _version.py")

    return mo.group(1)
